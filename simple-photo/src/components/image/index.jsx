import React from 'react';

import './image.css';
import topimg from './images/topimg.png';

const Image = () => {
    return(
        <img src={topimg} alt="BigPhoto" />
    )
}

export default Image;