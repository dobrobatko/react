import React from 'react';

import './textblock.css';

const Textblock = (props) => {
    return(
        <div className='textblock'>
            {props.text}
        </div>
    )
}

export default Textblock;