import React from 'react';

import './texttitle.css';

const Texttitle = (props) => {
    return(
        <p className='texttitle'>
            {props.text}
        </p>
    )
}

export default Texttitle;